window.MathJax = {
    loader: {load: ['[tex]/physics', '[tex]/color']},
    tex: {
        packages: {'[+]': ['physics', 'color']},
    },
};
window.onload = function () {

    // Fix Pandoc CHUNKEDHTML lang metatag
    function getMeta(metaName) {
        const metas = document.getElementsByTagName('meta');
        for (let i = 0; i < metas.length; i++) {
            if (metas[i].hasAttribute('lang')) {
                return metas[i].getAttribute('lang');
            }
        }
        return '';
    }
    // console.log(getMeta("lang"))
    var lang = getMeta("lang")
    if (lang != "") {
        document.getElementsByTagName("html")[0].setAttribute("lang",lang)
        document.getElementsByTagName("html")[0].setAttribute("xml:lang",lang)
    }

    // Sitenav (on page) layout readjustment
    var nav = document.getElementById("sitenav")
    if (nav) {
        var bf_nav = nav.parentElement.insertBefore(nav.cloneNode(), nav.nextSibling)
        bf_nav.setAttribute("id", "bfnav")
        bf_nav.append(nav.children[1])
        bf_nav.children[0].setAttribute("class", "bfnav")
        
        // Send next to right and previous to left
        bf_nav.children[0].prepend(bf_nav.children[0].children[1])
        
        var icons = document.createElement("img")
        icons.src = "/icons128.png"
        for (let i = 0; i < bf_nav.children[0].childElementCount; i++) {
            var button = bf_nav.children[0].children[i]
            if (button.childElementCount) {
                button.classList.add("button")
                button.id = button.children[1].getAttribute("rel")
                button.children[1].prepend(icons.cloneNode())
            }
        }

        // Get the path
        if (nav.children[0].children[0].textContent != "\n") {
            if (nav.children[0].children[0].children[1].textContent == nav.children[0].children[1].children[1].textContent) { nav.children[0].removeChild(nav.children[0].children[1]) }

            var path_links = nav.children[0].children
            for (var i = 0; i<nav.children[0].childElementCount; i++) {
                if (nav.children[0].children[0].textContent != "\n" ) {
                    nav.children[0].prepend("\u00A0>\u00A0")
                    nav.children[0].prepend(path_links[i])
                }
            }
        }

        // nav.parentElement.insertBefore(bf_nav, nav)
        document.body.appendChild(bf_nav)
        // bf_nav.parentElement.appendChild(bf_nav.cloneNode(true))
    }

    // Animations
    // Footer -62 px / -48 px
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        prevScrollpos = currentScrollPos;

    }

    // Fix long header numbers
    var h4s = document.getElementsByTagName("h4")
    for (let i = 0; i < h4s.length; i++) {
        let secns = h4s[i].children[0].textContent.split(".")
        h4s[i].children[0].textContent = secns[secns.length - 1]
    }
    // Create link for headers and section TOC
    var hs = document.querySelectorAll("h1, h2, h3, h4, h5, h6");
    // var ul = document.createElement("ul");
    // let hs_prev = hs[0];
    hs[0].style.marginTop = "0px"
    for (let i = 0; i < hs.length; i++) {
        // console.log(hs[i])
        let url = window.location.origin + window.location.pathname
        let id = hs[i].id
        if (hs[i].parentElement.tagName == "SECTION") {
            console.log(hs[i]);
            id = hs[i].parentElement.id
        }
        url += "#" + id
        let btn = document.createElement("DIV")
        btn.classList.add("copy-href")
        btn.setAttribute("data-href", url)
        btn.setAttribute("onclick", "copyHREF(event)")
        btn.textContent = "⧉" // "↗" //"📋"
        // hs[i].textContent += " "
        hs[i].appendChild(btn)

        // if (hs[i].tagName == "H3" || hs[i].tagName == "H4") {
        //     var a = document.createElement("a")
        //     var li = document.createElement("li")
        //     a.setAttribute("href", url)
        //     a.textContent = hs[i].textContent
        //     li.appendChild(a)
        //     ul.appendChild(li)
        // }

        // h_prev = hs[i]
    }
    // document.getElementById("margin").appendChild(ul)

    window.onresize = function() {
        var sidebar = document.getElementById("sidebar")
        if (window.innerWidth >= 1100) {
            sidebar.classList.remove("max-lift")
            sidebar.style.left = "0px"
        } else {
            sidebar.classList.remove("max-lift")
            sidebar.style.left = "-300px"
        }
        var ts_button = document.getElementById("stop-slides")
        if (ts_button.style.display == "none") {
            var tsb_button = document.getElementById("toggle-sb")
            tsb_button.style.right = getComputedStyle(ts_button)["right"]
        }
    }

    fetch('./sitemap.json')
        .then((response) => response.json())
        .then((sitemap) => {
            function getOrderedList(section, d) {
                var ol = document.createElement("ul")
                for (let i = 0; i < section.subsections.length; i++) {
                    var li = document.createElement("li")
                    var a = document.createElement("a")
                    a.href = section.subsections[i].section.path
                    var str = ""
                    if ( d > 1 && section.subsections[i].section.number ) {
                        str += section.subsections[i].section.number + ". " 
                    }
                    str += section.subsections[i].section.title
                    a.appendChild(document.createTextNode(str))
                    li.appendChild(a)

                    if (section.subsections[i].subsections.length != 0 && d > 1) {
                        var sub_ol = getOrderedList(section.subsections[i], d-1)
                        li.appendChild(sub_ol)
                    }
                    ol.appendChild(li)
                }
                return ol
            }
            var sidebar = document.getElementById("sb-toc")
            var sb_title = document.getElementById("sb-title")
            sb_title.appendChild(document.createTextNode(sitemap.section.title))
            sidebar.appendChild(getOrderedList(sitemap, 2))
            // Right TOC should go here from sitemap, getCurrent, getLits(current, depth)
        })
        .then(() => {
            var as = document.getElementById("sb-toc").getElementsByTagName("a")
            // console.log(as.length);
            for (let i = 0; i < as.length; i++) {
                // console.log(as[i].href);
                // console.log(window.location.href);
                // console.log(as[i].href == window.location.href);
                if (as[i].href == window.location.origin + window.location.pathname) {
                    as[i].parentElement.classList.add("current")
                    as[i].removeAttribute("href")
                }
            }
            var scrollpos = localStorage.getItem('sb-scrollpos');
            if (scrollpos) document.getElementById("sidebar").scrollTo(0, scrollpos);
        })
        .catch((err) => {
            var sb_t = document.getElementById("sb-title")
            var title = document.getElementById("title-block-header").children[0]
            console.log(title);
            sb_t.textContent = title.childNodes[0].textContent
            title.remove()

            document.getElementsByTagName("body")[0].classList.add("no-toc")
            
            // var title_div = document.createElement("div")
            // title_div.innerHTML = title.innerHTML
            // title.parentNode.replaceChild(title_div, title)
            // document.getElementById("sb-header").prepend(title_div)
            // console.log(title_text);
            // sb_title.appendChild(title_text)
        });
    
    // Remove slides' pauses
    ps = document.getElementsByTagName("p")
    for (var i = 0; i < ps.length; i++) {
        if (ps[i].textContent == ". . .") {
            ps[i].classList.add("hide")
        }
    }

    // Add tabindex=1 to certain elements
    var hints = document.getElementsByClassName("hint")
    // console.log(hints);
    for (let i = 0; i < hints.length; i++) {
        // console.log(hints[i]);
        hints[i].setAttribute("tabindex", 1)
    }
    var toggles = document.getElementsByClassName("toggle")
    for (let i = 0; i < toggles.length; i++) {
        toggles[i].setAttribute("tabindex", 1)
    }

    // Set slides shortcut url and get sec number
    var h1 = document.getElementsByTagName("h1")[0]
    var h2 = document.getElementsByTagName("h2")[0]
    var sec_num = ""
    var h1_h2_id = ""
    if (h1) {
        h1_h2_id = h1.id
        sec_num = h1.getAttribute("data-number")
    } else if (h2) {
        h1_h2_id = h2.id
        sec_num = h2.getAttribute("data-number")
    }
    if (h1_h2_id != "") {
        // console.log(h1_h2_id)
        var url_arr = window.location.href.split("/")
        var idtag = url_arr[url_arr.length-1]
        url_arr.splice(url_arr.length-1)
        url_arr[url_arr.length] = "slides.html#"
        url_arr[url_arr.length] = h1_h2_id
        // console.log(url_arr)
        document.getElementById("slides").children[0].setAttribute("data-src", url_arr.join("/"))
    } else {
        document.getElementById("play-slides").style.display = "none"
        window.onresize()
    }
    var longs = document.getElementsByClassName("long")
    if (longs.length == 0) {
        document.getElementById("toggle-long").style.display = "none"
    }

    // Pass section number to CSS
    var cont = document.getElementById("content")
    cont.setAttribute("style", "--sec-number: '" + sec_num + "'")
}

function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

function copyHREF(e) {
    // console.log(e.currentTarget)
    navigator.clipboard.writeText(e.currentTarget.getAttribute("data-href"))
        .then(
            alertPane("Link copied!")
        )
}

function alertPane(str) {
    var ap = document.getElementById("alert-pane")
    ap.textContent = str
    ap.classList.remove("transparent")
    ap.classList.add("visible")
    delay(1000).then(function () {
        ap.classList.add("transparent")
    })
    delay(3000).then(function () {
        ap.classList.remove("visible")
    })
    delay(3700).then(function () {
        ap.classList.remove("transparent")
    })
}

function toggleSidebar(e) {
    var sidebar = document.getElementById("sidebar")
    var sb_toggle = document.getElementById("sb-toggle")
    if (sidebar.classList.contains("max-lift")) {
        sidebar.classList.remove("max-lift")
        sidebar.style.left = "-300px"
        sb_toggle.style.display = "none"
    } else {
        sidebar.classList.add("max-lift")
        sidebar.style.left = "0px"
        sb_toggle.style.display = "block"
    }
}

function toggleSlides(e) {
    var slides = document.getElementById("slides")
    var stop_btn = document.getElementById("stop-slides")
    if (slides.classList.contains("visible")) {
        slides.classList.remove("visible")
        stop_btn.classList.add("hide")
    } else {
        slides.classList.add("visible")
        stop_btn.classList.remove("hide")
        slides.children[0].setAttribute("src", slides.children[0].getAttribute("data-src"))
    }
}

function setSummary() {
    var longs = document.getElementsByClassName("long")
    for (let i = 0; i < longs.length; i++) {
        longs[i].classList.add("hide")
    }
    localStorage.setItem('long', false)
    alertPane("Summary mode")
    document.getElementById("toggle-long").classList.add("selected")
}
function setExtended() {
    var longs = document.getElementsByClassName("long")
    for (let i = 0; i < longs.length; i++) {
        longs[i].classList.remove("hide")
    }
    localStorage.setItem('long', true)
    alertPane("Extended mode")
    document.getElementById("toggle-long").classList.remove("selected")

}
function toggleLong() {
    long_bool = localStorage.getItem("long")
    if (long_bool == "true") {
        setSummary()
    } else {
        setExtended()
    }
}

window.onbeforeunload = function(e) {
    localStorage.setItem('current-path', window.location.pathname)
    localStorage.setItem('scrollpos', window.scrollY);
    localStorage.setItem('sb-scrollpos', document.getElementById("sidebar").scrollTop);
};

// RECOVER PREVIOUS STATUS
document.addEventListener("DOMContentLoaded", function(event) { 
    var long = localStorage.getItem('long')
    if (long == "false") {
        setSummary();
    }
    var scrollpos = localStorage.getItem('scrollpos');
    var current_path = localStorage.getItem('current-path');
    if (current_path != "/" && window.location.pathname == "/") {
        window.location.replace(window.location.origin + current_path)
        if (scrollpos) window.scrollTo(0, scrollpos);
    }
    if (scrollpos && current_path == window.location.pathname) window.scrollTo(0, scrollpos);
});