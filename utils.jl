# using Pkg
# Pkg.activate(".")

using Pandoc, pandoc_crossref_jll, DataFrames

const PAbstractPath= Pandoc.FilePathsBase.AbstractPath
const CROSSREF_BIN_PATH = pandoc_crossref_jll.pandoc_crossref_path
const HTML_DIR = "_html/"
const TEX_DIR = "_tex/"

function build()
    for r_dir in [HTML_DIR, TEX_DIR]
        if isdir(r_dir) 
            for dir in filter(isdir, readdir(r_dir, join=true))
                rm(dir, recursive=true)
            end
        else
            mkdir(r_dir)
        end
    end

    df_paths = getDocs()
    
    
    doc404 = Pandoc.Document(
        readMultipleAsString(["md/config.md", "md/fourzerofour.md"])
    )
    createHTML(
        doc404,
        "_html/404.html"
    )

    # index_doc.meta.content["itinerary"].content["c"].content["A"].content["c"].content["rm"].content["c"].content[1].content["c"].content if str is not empty
    # index_doc.meta.content["itinerary"].content["c"].content["A"].content["c"].content["perm"].content["c"].content == "" if string is empty    
    index_doc = Pandoc.Document(
        readMultipleAsString(["md/config.md", "md/index.md"])
    )
    itry_meta = getMetaDict(index_doc, "itinerary") # Dict with URLs as keys
    createHTML(
        index_doc,
        "_html/index.html"
    )
    for (url, dic_rm_perm) in itry_meta

        df_loc = copy(df_paths) 

        rm_vec = parseIdentifier.(split(dic_rm_perm["rm"], ",", keepempty = false))
        perm_vec = map((str_vec) -> parseIdentifier.(str_vec), split.(split(dic_rm_perm["perm"],",", keepempty = false),"|"))

        println("Building for $(url)...")

        for el in rm_vec
            @show el
            # el = (n, m) | (n, missing) single files
            # el = (n, true) | (n, false) all OR only folder
            if typeof(el[2]) == Int # single sec file
                filter!([:sec, :subsec] => (sec, subsec) -> !(sec == el[1] && subsec == el[2]), df_loc)
            elseif el[2] # all files with sec
                filter!([:sec] => (sec) -> !(sec == el[1]), df_loc)
            else # files in sec folder
                filter!([:sec, :subsec] => (sec, subsec) -> !(sec == el[1] && subsec != -1), df_loc)
            end
        end

        for (from, to) in perm_vec
            typeof(from[2]) == typeof(to[2]) || error("In $url itinerary (Permutation): $from and $to are not compatible pairs") 
            if typeof(from[2]) == Int  # single file
                for i in 1:length(df_loc.sec)
                    if df_loc.sec[i] == from[1] && df_loc.subsec[i] == from[2]
                        df_loc.sec[i] = to[1]
                        df_loc.subsec[i] = to[2]
                    elseif df_loc.sec[i] == to[1] && df_loc.subsec[i] == to[2]
                        df_loc.sec[i] = from[1]
                        df_loc.subsec[i] = from[2]
                    end
                end
            elseif from[2] == to[2]
                if from[2] # all files with sec
                    for i in 1:length(df_loc.sec)
                        if df_loc.sec[i] == from[1]
                            df_loc.sec[i] = to[1]
                        elseif df_loc.sec[i] == to[1]
                            df_loc.sec[i] = from[1]
                        end
                    end
                else # files in sec folder
                    for i in 1:length(df_loc.sec)
                        if df_loc.subsec[i] != -1
                            if df_loc.sec[i] == from[1]
                                df_loc.sec[i] = to[1]
                            elseif df_loc.sec[i] == to[1]
                                df_loc.sec[i] = from[1]
                            end
                        end
                    end
                end
            else # not compatible pairs
                error("In $url itinerary (Permutation): $from and $to are not compatible pairs")
            end
        end
        
        sort!(df_loc, [:sec, :subsec])

        @show df_loc

        doc_str = read("md/config.md", String)*"\n\n"

        for row in eachrow(df_loc)
            if row.subsec <= 0
                doc_str *= read(row.file, String)*"\n\n"
            else
                doc_str *= run(Pandoc.Converter(input=read(row.file, String), to="markdown", shift_heading_level_by=1))*"\n\n"
            end
        end

        # doc_str = readMultipleAsString(["md/config.md", df_loc.file...])
    
        # Set basepath on assets 
        doc = Pandoc.Document(doc_str)
        
        chunkedHTML(doc, "_html/"*url, "es-ES")
        # createHTML(doc, HTML_DIR * url * "/pdf-" * "es.html")
        createSlidesHTML(doc, "_html/"*url*"/slides.html")
        # cp("layouts/slideous","_html/"*url*"/slideous", force=true)
    end

    cp("img/", "_html/img", force=true)
    
    cp("layouts/eduki.css","_html/eduki.css", force=true)
    cp("layouts/eduki.js","_html/eduki.js", force=true)
    cp("layouts/favicon.png","_html/favicon.png", force=true)
    cp("layouts/icons128.png", "_html/icons128.png", force=true)

    nothing

end

function readMultipleAsString(paths)
    mapreduce((str)->str*"\n\n", *, vcat(read.(paths, String)))
end

function chunkedHTML(doc, output_dir, lang)
    run(Pandoc.Converter(
        input = doc,
        to = "chunkedhtml", 
        from = "markdown+link_attributes",
        split_level = 2,
        output = output_dir,
        toc = true,
        toc_depth = 2,
        standalone=true,
        mathjax=true,
        number_sections=true,
        filter=["$(CROSSREF_BIN_PATH)"],
        include_in_header = PAbstractPath["./layouts/header-include.html", createMetaLangTagFile(lang)],
        include_before_body = PAbstractPath["./layouts/include-before.html"],
        include_after_body = PAbstractPath["./layouts/include-after.html"],
    ))
end

function createHTML(doc, output)
    run(Pandoc.Converter(
        input = doc,
        to = "html", 
        output = output,
        standalone=true,
        mathjax=true,
        include_in_header = PAbstractPath["./layouts/header-include.html"],
        include_before_body = PAbstractPath["./layouts/include-before.html"],
        include_after_body = PAbstractPath["./layouts/include-after.html"],
    ))
end
    
function createSlidesHTML(doc, output)
    # Include section TOC
    doc = includeSubTocs(doc)
    run(Pandoc.Converter(
        input = doc,
        to = "revealjs",
        output = output,
        slide_level = 4,
        standalone=true,
        mathjax=true,
        filter=["$(CROSSREF_BIN_PATH)"],
        include_in_header = PAbstractPath["./layouts/header-include-slides.html"],
        # include_before_body = PAbstractPath["./layouts/include-before-slides.html"],
        # include_after_body = PAbstractPath["./layouts/include-after-sildes.html"],
    ))
end

function createTEX(doc, output_dir, lang)
    run(Pandoc.Converter(
        input = doc,
        to = "latex", 
        from = "markdown+link_attributes",
        output = output_dir*lang*".tex",
        toc = true,
        toc_depth = 2,
        standalone=true,
        mathjax=true,
        number_sections=true,
        filter=["$(CROSSREF_BIN_PATH)"],
        include_in_header = PAbstractPath["./layouts/header.tex"]
    ))
end

function getDocs()
    src_dir = readdir("md", join = true)
    
    filepaths = filter(isValidMD, src_dir)
    secs = map((str)-> parse(Int, split(splitpath(str)[end],".")[1]), filepaths)
    subsecs = fill(-1, length(secs))

    for sec_dir in filter(isValidDir, src_dir)
    
        sec_filepaths = filter(isValidMD, readdir(sec_dir, join=true))
        sec_secs = fill(parse(Int,split(splitpath(sec_dir)[end],".")[1]), length(sec_filepaths))
        sec_subsecs = map((str)-> parse(Int, split(splitpath(str)[end],".")[1]),sec_filepaths)
        
        push!(filepaths, sec_filepaths...)
        push!(secs, sec_secs...)
        push!(subsecs, sec_subsecs...)
    
    end

    df = DataFrame(sec = secs, subsec = subsecs, file = filepaths)
    sort(df, [:sec, :subsec])
end

function isValidMD(filepath)
    filename = splitpath(filepath)[end]
    isdir(filename) && return false 
    splitedpath = split(filename,".")
    if aredigits(splitedpath[1]) && splitedpath[end] == "md"
        return true
    else
        return false
    end
end

function isValidDir(dirpath)
    isdir(dirpath) && aredigits(split(splitpath(dirpath)[end],".")[1])
end

function aredigits(str)
    str == "" && return false
    for c in str
        isdigit(c) && continue
        return false
    end
    true
end

function createMetaLangTagFile(lang)
    f = tempname()
    write(f, "<meta lang=\"$(lang)\">")
    f
end

function getMetaDict(doc, label)
    getDictOrList(doc.meta.content[label])
end

function getDictOrList(content)
    obj = content.content["c"].content
    d = Dict()
    if hasfield(typeof(obj), :keys)
        for (key, value) in obj
            d[key] = getDictOrList(value)
        end
    elseif typeof(obj) == String
        return obj
    else
        return getDictOrList(obj[1])
    end
    d   
end

function parseIdentifier(str)
    str[end] == '*' && return (parse(Int, str[1:end-1]), true)
    str[end] == '.' && return (parse(Int, str[1:end-1]), false)
    nm = parse.(Int, split(str, "."))
    if length(nm) == 1
        return (nm[1], -1)
    else
        return (nm[1], nm[2])
    end
end

function includeSubTocs(doc)
    function gatherList(blocks, ind)
        # It starts from a hit in 1st level header 
        # until next 1st level header
        list = Vector{Pandoc.Block}[]
        for block in blocks[ind+1:end]
            if block isa Pandoc.Header 
                if block.level == 2
                    # add to list
                    push!(list, [Pandoc.Para(Pandoc.Inline[Pandoc.Link(Pandoc.Attr("", String[], Tuple{String, String}[]), block.content, Pandoc.Target("#" * block.attr.identifier, ""))])])
                elseif block.level == 1
                    return Pandoc.OrderedList(Pandoc.ListAttributes(1, Pandoc.ListNumberStyle.Decimal, Pandoc.ListNumberDelim.Period),list)
                end
            end
        end
        Pandoc.OrderedList(Pandoc.ListAttributes(1, Pandoc.ListNumberStyle.Decimal, Pandoc.ListNumberDelim.Period),list)
    end
    doc_blocks = Pandoc.Block[]
    ind = 0
    for block in doc.blocks
        ind += 1
        push!(doc_blocks, block)
        if block isa Pandoc.Header
            if block.level == 1
                push!(doc_blocks, gatherList(doc.blocks, ind))
            end
        end
    end
    doc.blocks = doc_blocks
    doc
end

function clearSlideMarcks!(doc)
    filter!((b) -> !(isa(b, Pandoc.HorizontalRule) || isDots(b)), doc.blocks)
end

function isDots(block)
    isa(block, Pandoc.Para) || return false
    isa(block.content, Vector{Pandoc.Inline}) || return false
    5 == length(block.content) || return false
    "." == block.content[1].content || return false
    "." == block.content[3].content || return false
    "." == block.content[5].content || return false
    isa(block.content[2], Pandoc.Space) || return false
    isa(block.content[4], Pandoc.Space) || return false
    true    
end

function latexEnvirontments!(doc)
    b_str(x) = Pandoc.RawBlock("latex", "\\begin{$x}")
    e_str(x) =  Pandoc.RawBlock("latex", "\\end{$x}")
    blocks = []
    for block in doc.blocks
        if block isa Pandoc.Div
            push!(blocks, b_str(block.attr.classes[1]))
            push!(blocks, block.content...)
            push!(blocks, e_str(block.attr.classes[1]))
        else
            push!(blocks, block)
        end
    end
    doc.blocks = blocks
end

LATEX_ENVS = ["exercise", "alert", "warning", "example", "note"]

function latexEnvFromDIV(div)
    in(div.attr.classes[1], LATEX_ENVS) || return walkAndSubs(latexEnvFromDIV, Pandoc.Div, div.content)
    b_str(x) = Pandoc.RawBlock("latex", "\\begin{$x}")
    e_str(x) = Pandoc.RawBlock("latex", "\\end{$x}")
    return [
        b_str(div.attr.classes[1]),
        walkAndSubs(latexEnvFromDIV, Pandoc.Div, div.content)...,
        e_str(div.attr.classes[1])
    ]
end
    
LATEX_MACROS = ["hint"]
    
function latexMacroFromSPAN(span)
    in(span.attr.classes[1], LATEX_MACROS) || return walkAndSubs(latexMacroFromSPAN, Pandoc.Span, span.content)
    b_str(x) = Pandoc.RawInline("latex", "\\$x{")
    e_str() = Pandoc.RawInline("latex", "}")
    return [
        b_str(span.attr.classes[1]),
        walkAndSubs(latexMacroFromSPAN, Pandoc.Span, span.content)...,
        e_str()
    ]
end

function walkAndSubs(f, elisa, els)
    o = []
    for el in els
        if isa(el, elisa)
            push!(o, f(el)...)
        else
            if hasproperty(el, :content) && isa(el.content, Vector)
                el.content = walkAndSubs(f, elisa, el.content)
            end
            push!(o, el)
        end
    end
    o
end
