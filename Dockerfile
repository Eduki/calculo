FROM arm64v8/ubuntu:22.04

# Docker image for building Eduki.edu content
# We need Julia for managing files
# We need Pandoc to compile to HTML and TEX
# We need pandoc-xnos filter for references

RUN apt-get update && apt-get install -y \
  wget \
  pandoc \
  python3 \
  python3-pip \
  git \
  texlive-base

RUN pip

RUN wget https://julialang-s3.julialang.org/bin/linux/aarch64/1.9/julia-1.9.4-linux-aarch64.tar.gz && \
  tar xvzf julia-1.9.4-linux-aarch64.tar.gz && \
  mv julia-1.9.4 /usr/local/julia && \
  rm julia* 

RUN pip install pandoc-secnos git+https://github.com/nandokawka/pandoc-xnos@284474574f51888be75603e7d1df667a0890504d#egg=pandoc-xnos

ENV PATH="{$PATH}:/usr/local/julia/bin:/root/.local/bin"

RUN julia -e 'import Pkg; \
  Pkg.add(url="https://github.com/iagobaapellaniz/Pandoc.jl", rev="using"); \
  Pkg.add("DataFrames"); \
  Pkg.add("LiveServer")'

RUN mkdir /home/wd
WORKDIR /home/wd

EXPOSE 8000

