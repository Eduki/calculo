# Operaciones con números complejos {#sec:nc-oper}

Las 4 diferentes potencias de $\i^{n}$ son $\i^{1}=\i$, $\i^{2}=-1$, $\i^{3}=-\i$ y $\i^{4}=1$. En general, $\i^{4r+s}=\i^{s}$, para $\forall r,s \in \mathbb{Z}$.

## Operaciones básicas

### Suma/resta:
Para $z=a+\i b$ y $z'=a'+\i b'$ tenemos que $z+z'=(a+a')+\i (b+b')$.

### Producto:

- Para $z=a+\i b$ y $z'=a'+\i b'$ tenemos que $zz'=(aa'-bb')+\i (ab'+ba')$.
- Para $z=\varrho_\theta$ y $z'=\varrho'_{\theta'}$ tenemos que $zz'=(\varrho\varrho')_{\theta + \theta'}$

------------------

### Cociente:

- Para $z=a+\i b$ y $z'=a'+\i b'$ tenemos que 
$$\frac{z'}{z}=\frac{z'z^*}{zz^*}=\frac{(a'a+b'b)+\i (ab'-ba')}{a^2+b^2}.$$
- Para $z=\varrho_\theta$ y $z'=\varrho_{\theta'}$ tenemos que $zz'=(\varrho/\varrho')_{\theta - \theta'}$

### Potencia con un entero:

Para $z=\varrho_\theta$ tenemos que $z^n=(\varrho^n)_{n\theta}$

[Para un $n$ entero y $\abs{z}=1$, utilizando las reglas de las potencias para números complejos, tenemos que, 
$$
(\cos(\theta) + \i\sin(\theta))^n = \cos(n\theta) + \i \sin(n\theta).
$$
Nótese que esta formula vale tanto para los enteros negativos como para los positivos.
]{.theorem .important-definition data-label="Formula de De Moivre: "}

## El conjugado

El conjugado de un número $z$ se escribe como $z^*$ o $\bar{z}$.

- Si $z=a+\i b \Rightarrow z^*=a-\i b$
- Si $z=\varrho_\theta \Rightarrow z^*=\varrho_{-\theta}$
- $2\re(z) = z+z^*$
- $2i\im(z) = z-z^*$
- $\rho^2 = zz^*$

## Operaciones con soluciones múltiples

Para las siguientes operaciones hay que tener cuidado con los argumentos puesto que tienen múltiples valores, $\theta = \theta_\mathrm{p}+2\pi k$ para $\forall k \in \mathbb{Z}$.

### Raíz con un entero

Para $z=\varrho_\theta$ tenemos que 
$$z^n=(\sqrt[n]\varrho)_{\frac{\theta_\mathrm{p}+2\pi k}{n}}\text{ para }\forall k\in\{0,1,\dots,n-1\}.$$ 

> Si se pide el argumento principal, habrá que comprobar si los argumentos están en el rango $(-\pi,\pi]$.

### Logaritmo

Para $z=\varrho\e^{\i\theta}$ tenemos que
$$ 
\begin{split}
    \ln(z) 
    & = \ln(\varrho\e^{\i\theta})=\ln(\varrho)+\ln(\e^{\i\theta}) \\
    & = \ln(\varrho)+\i(\theta_\mathrm{p}+2\pi k) \text{ para }\forall k \in \mathbb{Z}.
\end{split}
$$
El logaritmo obtenido con $\theta_\mathrm{p}$ tiene como nombre _logaritmo principal_.

* El logaritmo coge infinitos valores en la vertical imaginaria!
* Para los logaritmos en otra base se utiliza la identidad $\log_{z_2}(z_1)=\frac{\ln(z_1)}{\ln(z_2)}$.

### Potencia compleja

Para $z=\varrho\e^{\i\theta}$ elevado a $z_1=a+\i b$ 
$$
\begin{split}
    z^{z_1}
    &=\e^{\ln(z^{z_1})}
    =\e^{z_1\ln(z)}\\
    &=\e^{(a+\i b)(\ln(\varrho)+\i(\theta_\mathrm{p}+2\pi k))}\\
    &=\e^{(a\ln(\varrho)- b(\theta_\mathrm{p}+2\pi k)}\e^{i(b\ln(\varrho)+a(\theta_\mathrm{p}+2\pi k))}
    \text{ para }\forall k \in \mathbb{Z}
\end{split}
$$

## Ejercicios {#ex:operaciones-complejos .exercises}

1. Calcúla el producto, $z_1z_2$, de los siguientes pares de números complejos $(z_1=3-\i 2, z_2 = 1+\i 3)$, $(z_1=2\e^{-\i\pi/4}, z_2 = 1+\i 3)$ y ($z_1=3-\i 2, z_2 = 1+\i 3$). Repite el ejercicio, pero en este caso calcula el cociente $z_1/z_2$.

1. $\displaystyle\frac{12+8\i}{2-3\i}+\frac{52+15\i}{13\i}$ 
[$15/13$]{.hint}

1. Resolver $z^3z^*=-1$ [$z = ± \i$]{.hint}

1. $\ln (1+\i)$ [$\ln(2)/2+\i\pi(1/4+2k)\quad\forall k\in\mathbb{Z}$]{.hint}

1. $(1-\i)^{8n}$ para $n\in\mathbb{N}$ 
[$2^{4n}$]{.hint}

1. $\ln(1-\i\sqrt{3})$ 
[$\ln(2) - i (π/3+2\pi k)\quad\forall k\in\mathbb{Z}$]{.hint}

1. $\ln(\frac{13}{1+\i})$ 
[$\ln(13)- \ln(2)/2 +\i(2\pi k - \pi/4)$ ]{.hint}

1. $1^{1/3+\i}$ 
[$\e^{-2\pi k}\e^{\i 2\pi k/3}\quad\forall k\in\mathbb{Z}$]{.hint}

1. $(1-\sqrt{3}\i)^2$ 
[$-2(1+\sqrt{3}\i)$]{.hint}

1. $\displaystyle\lpar\frac{34}{(1-\i 4)(5+\i 3)}\rpar^2$ 
[$\frac{2}{289}(-240+161\i)$]{.hint}

1. $(-2\sqrt{3}-\i 2)^{1/4}$ [
$\sqrt{2}_{\frac{-5 + 12k}{24}\pi}\quad\forall k\in\{-1,0,1,2\}$]{.hint}

1. $(-1)^\i$ 
[$\e^{-\pi+2\pi k}\quad\forall k\in\mathbb{Z}$ ]{.hint}

1. $(1+\i\sqrt{3})^{-0.5-\i0.5}$
[$\e^{\pi/6 -\ln(2)/2 + \pi n}\e^{i(\ln(2)/2-\pi/6-\pi n)}\quad\forall n\in\mathbb{Z}$]{.hint}

1. $(1+\i)^8$ 
[$16$]{.hint}

1. $2(\cos(\pi/6)+\i\sin(\pi/6))\cdot 3(\cos(\pi/12)-\i \sin(\pi/12))$
[$6(\cos(\pi/12)+\i\sin(\pi/12))$]{.hint}
