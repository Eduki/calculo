# Limites

## Definición del límite

Un **punto de acumulación** en $a$ se define como un conjunto de valores tanto mayores como menores de $a$ excluyendo $a$,
$$
\varepsilon^*(a,\delta) = \{x\in\mathbb{R} \mid a{-}\delta < x < a \lor a<x<a{+}\delta\}.
$$

Para una función $f$ donde $\dom(f)=X$ y un punto de acumulación $\varepsilon^*(a,\delta) \cap X \neq \varnothing$ tenemos que el límite de la función es
$$
\lim_{x\rightarrow a} f(x) = l \Longleftrightarrow \forall \varepsilon >0, \exists \delta>0 \mid \forall x\in \varepsilon^*(a,\delta) \cap X \Rightarrow f(x)\in\varepsilon^*(l, \varepsilon),
$$
o de otra manera
$$
\lim_{x\rightarrow a} f(x) = l \Longleftrightarrow \forall \varepsilon >0, \exists \delta>0 \mid \forall x\in X \land \abs{x-a}<\delta \Rightarrow \abs{f(x)-l}<\varepsilon.
$$

::: {.note .example data-label="Ejemplo"}
Calcula el límite $\displaystyle \lim_{x\rightarrow 3}x^2-5x = 3^2 - 5\cdot3 = -6.$
:::

### Límites laterales

Considerando valores solo mayores que $a$, el límite se conoce como **límite por la derecha**
$$
\lim_{x\rightarrow a^+} f(x) = l^+ \Longleftrightarrow \forall \varepsilon >0, \exists \delta>0 \mid \forall x\in X \land a< x< a {+} \delta \Rightarrow \abs{f(x)-l^+}<\varepsilon.
$$
Considerando valores solo menores que $a$, el límite se conoce como **límite por la izquierda**
$$
\lim_{x\rightarrow a^-} f(x) = l^- \Longleftrightarrow \forall \varepsilon >0, \exists \delta>0 \mid \forall x\in X \land a {-} \delta < x< a \Rightarrow \abs{f(x)-l^-}<\varepsilon.
$$

[El límite de una función $f$ tiene como valor $l$ si y solamente si los límites laterales existen, son iguales entre sí y coinciden con el valor $l$,
$$
\lim_{x\rightarrow a} f(x) = l \Leftrightarrow \lim_{x\rightarrow a^-} f(x) = \lim_{x\rightarrow a^+} f(x) = l.
$$
]{.theorem data-label="Teorema: "}

::: {.note .example data-label="Ejemplo"}
Para calcular el límite $\displaystyle \lim_{x\rightarrow 0}\frac{6\abs{x}}{x}$
$$
\begin{cases}
    \displaystyle \lim_{x\rightarrow 0^+}\frac{6x}{x} = \lim_{x\rightarrow 0^+} 6 = 6 \\
    \displaystyle \lim_{x\rightarrow 0^-}\frac{6(-x)}{x} = \lim_{x\rightarrow 0^-} (-6) = -6
\end{cases}
$$
vemos que los límites no coinciden. Por lo tanto $\nexists \displaystyle \lim_{x\rightarrow 0}\frac{6\abs{x}}{x}.$
:::

### Límites infinitos

Los límites infinitos se definen mediante
$$
\lim_{x\rightarrow a} f(x) = +\infty \Longleftrightarrow \forall K >0, \exists \delta>0 \mid \forall x\in X \land \abs{x-a}<\delta \Rightarrow f(x)>K,
$$
para los límites $+\infty$, y
$$
\lim_{x\rightarrow a} f(x) = -\infty \Longleftrightarrow \forall K >0, \exists \delta>0 \mid \forall x\in X \land \abs{x-a}<\delta \Rightarrow f(x)<-K,
$$
para los límites $-\infty$.
De la misma manera se definen los límites infinitos laterales.

::: {.note .example data-label="Ejemplo"}
Calcula $\displaystyle \lim_{x\rightarrow 3} \frac{1}{x^2 - 6x + 9}$. De nuevo calculamos los límites laterales
$$
\begin{cases}
\displaystyle \lim_{x\rightarrow 3^+} \frac{1}{x^2 - 6x + 9} = \lim_{x\rightarrow 3^+} \frac{1}{(x-3)^2} = +\infty, \\
\displaystyle \lim_{x\rightarrow 3^-} \frac{1}{x^2 - 6x + 9} = \lim_{x\rightarrow 3^-} \frac{1}{(x-3)^2} = +\infty. \\
\end{cases}
$$
De modo que $\exists\displaystyle \lim_{x\rightarrow 3} \frac{1}{x^2 - 6x + 9} = +\infty.$ 
:::

---

::: {.note .example data-label="Ejemplo"}
Calcula $\displaystyle \lim_{x\rightarrow 3} \frac{1}{x^2 - 3x}$. De nuevo calculamos los límites laterales
$$
\begin{cases}
\displaystyle \lim_{x\rightarrow 3^+} \frac{1}{x^2 - 3x} = \lim_{x\rightarrow 3^+} \frac{1}{(x-3)x} = +\infty, \\
\displaystyle \lim_{x\rightarrow 3^-} \frac{1}{x^2 - 3x} = \lim_{x\rightarrow 3^-} \frac{1}{(x-3)x} = -\infty. \\
\end{cases}
$$
De modo que $\nexists\displaystyle \lim_{x\rightarrow 0^+} \frac{1}{x^2 - 6x + 9}.$ 
:::

### Límites en el infinito
Las definiciones de los límites en el infinito son: para el límite en $+\infty$ que adquiere el valor $l$
$$
\lim_{x\rightarrow +\infty} f(x) = l \Longleftrightarrow \forall \varepsilon > 0, \exists H>0 \mid x>H \Rightarrow \abs{f(x)-l}<\varepsilon,
$$
para el límite en $-\infty$ que adquiere el valor $l$
$$
\lim_{x\rightarrow -\infty} f(x) = l \Longleftrightarrow \forall \varepsilon > 0, \exists H>0 \mid x<-H \Rightarrow \abs{f(x)-l}<\varepsilon,
$$
para el límite en $+\infty$ que adquiere el valor $+\infty$
$$
\lim_{x\rightarrow +\infty} f(x) = +\infty \Longleftrightarrow \forall K > 0, \exists H>0 \mid x>H \Rightarrow f(x)>K,
$$
y analogamente para $\displaystyle\lim_{x\rightarrow +\infty} f(x) = -\infty$, $\displaystyle\lim_{x\rightarrow -\infty} f(x) = +\infty$ y $\displaystyle\lim_{x\rightarrow -\infty} f(x) = -\infty.$

> En los límites en el infinito no hay límites laterales.

::: {.note .example data-label="Ejemplo"}
Calcula el límite $\displaystyle \lim_{x \rightarrow \infty} \frac{7x + 1}{x} = \lim_{x \rightarrow \infty} \frac{7x}{x} + \lim_{x \rightarrow \infty}\frac{1}{x}=\lim_{x \rightarrow \infty} 7 + 0 = 7$

Calcula el límite $\displaystyle \lim_{x \rightarrow -\infty} 7x + 1 = -7\infty + 1 = -\infty.$
:::

## Propiedades de los límites

Límite | Equivalente | Observaciones
---|---|---
$\displaystyle \lim_{x\rightarrow c} k f(x)$ | $\displaystyle k \lim_{x\rightarrow c} f(x)$ | 
$\displaystyle \lim_{x\rightarrow c} (f(x) + g(x))$ | $\displaystyle \lim_{x\rightarrow c} f(x) + \lim_{x\rightarrow c}g(x)$
$\displaystyle \lim_{x\rightarrow c} (f(x)  g(x))$ | $\displaystyle \lim_{x\rightarrow c} f(x) \cdot \lim_{x\rightarrow c}g(x)$
$\displaystyle \lim_{x\rightarrow c}\frac{f(x)}{g(x)}$ | $\displaystyle \frac{\lim_{x\rightarrow c} f(x)}{\lim_{x\rightarrow c}g(x)}$ | Solo si $\lim_{x\rightarrow c}g(x)\neq 0$
$\displaystyle \lim_{x\rightarrow c}\ln(f(x))$ | $\displaystyle \ln(\lim_{x\rightarrow c}f(x))$ | Solo si $\lim_{x\rightarrow c}f(x) > 0$
$\displaystyle \lim_{x\rightarrow c} a^{f(x)}$ | $\displaystyle a^{\lim_{x\rightarrow c} f(x)}$ | Solo si $a>0$
$\displaystyle \lim_{x\rightarrow c} f(x)^{g(x)}$ | $\displaystyle \lim_{x\rightarrow c} f(x)^{\lim_{x\rightarrow c}g(x)}$ | Solo si $\lim_{x\rightarrow c} f(x)>0$

---

* _Regla del Sandwich_: Si $f(x)\leqs g(x) \leqs h(x)$ en el entorno de $a$, si $\lim_{x\rightarrow a} f(x) = \lim_{x\rightarrow a} h(x)=l$ entonces $\lim_{x\rightarrow a} g(x) = l.$

* Si $f(x)$ está acotada en el entorno de $a$ y $\lim_{x\rightarrow a} g(x)=0$ entonces $\lim_{x\rightarrow a}(f(x)g(x))=0$

. . .

::: {.note .example data-label="Ejemplo"}
Para calcular el límite $\displaystyle \lim_{x \rightarrow \infty} \sin(x) \e^{-x}$ podemos hacerlo de dos maneras:

* $-\e^{-x} \leqslant \sin(x) \e^{-x} \leqslant \e^{-x}.$

* $\sin(x)$ es una función acotada, y $\displaystyle \lim_{x \rightarrow \infty} \e^{-x} = 0.$

Sabiendo que $\displaystyle \lim_{x \rightarrow \infty} \pm\e^{-x} = 0$ concluimos que $\displaystyle \lim_{x \rightarrow \infty} \sin(x) \e^{-x} = 0.$
:::

. . .

::: {.note .example data-label="Ejemplo"}
Para $x\rightarrow 0^+$ y sabiendo que $\sin(x) \leqslant x \leqslant \tan{x}$, tenemos que $1\leqslant \frac{x}{\sin(x)} \leqslant \frac{1}{\cos(x)}$ despues de dividir todo por $\sin(x)$.
Por lo tanto 
$$
\lim_{x\rightarrow 0^+} \frac{x}{\sin(x)} = 1
$$
sabiendo que $\lim_{x\rightarrow 0^+} \cos(x) = 1.$ Para $x\rightarrow 0^-$ tenemos que $\sin(x) \geqslant x \geqslant \tan{x}$ y los límites coinciden, $\displaystyle \exists \lim_{x\rightarrow 0} \frac{x}{\sin(x)} = 1.$
:::

### Operaciones entre límites con infinitos y indeterminaciones

Si alguno de los límites es $\infty$ o $0$ y el otro es $l$, $\infty$ o $0$ el límite será:

--------------------------------   ---------------                            --------------
$\pm\infty\pm\infty = \pm\infty$   $\pm\infty\cdot(\pm\infty) = +\infty$      $\pm\infty\cdot(\mp\infty) = -\infty$ 
$\pm\infty\pm l = \pm\infty$       $\pm\infty \cdot l = \pm\infty$ si $l>0$   $\pm\infty \cdot l = \mp\infty$ si $l<0$
$l^\infty=\infty$ si $l>1$         $l^\infty=0$ si $0\leqs l<1$               $l/(\pm\infty) = 0$       
$l^{-\infty}=0$ si $l>1$           $l^{-\infty}=\infty$ si $0\leqs l<1$       $l/0 =\mp\infty$ si $l\lessgtr 0$                                                     
---- -- ---

. . .

Si alguno de los límites es $\infty$ o $0$ y el otro es $l$, $\infty$ o $0$ el límite será indeterminado en los siguientes casos:

---------------------  ---------------         --------------
$\pm\infty\mp\infty$   $\infty/\infty$         $0/0$ 
$0\cdot\infty$         $0^0$ o $\infty^0$      $1^\infty$     
---- -- ---

### Infinitos e infinitesimos 

* Si $\lim_{x\rightarrow a} f(x) = 0$ decimos que $f$ es un _infinitésimo_ en el punto $a$.
* Si $\lim_{x\rightarrow a} f(x) = \pm \infty$ decimos que $f$ es un _infinito_ en el punto $a$.

Tanto los infinitésimos como infinitos admiten el cálculo de límite recíproco (o inverso).
Un límite infinitésimo se convierte en infinito y viceversa al calcular dicho límite recíproco.

$$
    \lim_{x\rightarrow a} f(x) = 0 \Rightarrow \lim_{x\rightarrow a} \frac{1}{f(x)} = \pm\infty
$$
$$
    \lim_{x\rightarrow a} f(x) = \pm\infty \Rightarrow \lim_{x\rightarrow a} \frac{1}{f(x)} = 0
$$

---

A la hora de tratar de resolver las indeterminaciones, debemos clasificar los diferentes infinitésimos e infinitos.

* $f$ es infinitésimo de mayor orden que $g$ si $\displaystyle \lim_{x\rightarrow a} \frac{f(x)}{g(x)} = 0.$

* $f$ es infinitésimo de menor orden que $g$ si $\displaystyle \lim_{x\rightarrow a} \frac{f(x)}{g(x)} = \pm\infty.$
* $f$ y $g$ son infinitésimos del mismo orden si $\displaystyle \lim_{x\rightarrow a} \frac{f(x)}{g(x)} = l$ donde $l\neq0.$ Si $l=1$ se dice que $f$ y $g$ son **infinitésimos equivalentes**.

. . .

* $f$ es infinito de mayor orden que $g$ si $\displaystyle \lim_{x\rightarrow a} \frac{f(x)}{g(x)} = \pm\infty.$

* $f$ es infinito de menor orden que $g$ si $\displaystyle \lim_{x\rightarrow a} \frac{f(x)}{g(x)} = 0.$
* $f$ y $g$ son infinitos del mismo orden si $\displaystyle \lim_{x\rightarrow a} \frac{f(x)}{g(x)} = l$ donde $l\neq0.$ Si $l=1$ se dice que $f$ y $g$ son **infinitos equivalentes**.

---

Orden                Infinitésimo $x\rightarrow a$  Infinitésimo $x\rightarrow \infty$  Infinito $x\rightarrow a$  Infinito $x\rightarrow \infty$
----                 ----                           ----                                ----                       ----
Principal (orden 1)  $x-a$                          $\dfrac{1}{x}$                      $\dfrac{1}{x-a}$           $x$
$2$                  $(x-a)^2$                      $\dfrac{1}{x^2}$                    $\dfrac{1}{(x-a)^2}$       $x^2$
$\vdots$             $\vdots$                       $\vdots$                            $\vdots$                   $\vdots$ 
$r$                  $(x-a)^r$                      $\dfrac{1}{x^r}$                    $\dfrac{1}{(x-a)^r}$       $x^r$

. . .

Basandonos en esta tabla tenemos que ...

* Un límite infinitesimo es de orden $r$ en $a$ si y solo si $\displaystyle \lim_{x\rightarrow a} \frac{f(x)}{(x-a)^r}=l$ y $l\ne 0$.

* Un límite infinitesimo es de orden $r$ en $\infty$ si y solo si $\displaystyle \lim_{x\rightarrow a} f(x)x^r=l$ y $l\ne 0$.
* Un límite infinito es de orden $r$ en $a$ si y solo si $\displaystyle \lim_{x\rightarrow a} f(x)(x-a)^r=l$ y $l\ne 0$.
* Un límite infinito es de orden $r$ en $\infty$ si y solo si $\displaystyle \lim_{x\rightarrow a} \frac{f(x)}{x^r}=l$ y $l\ne 0$.

### Equivalencias: Infinitésimos

Ya hemos visto la equivalencia entre $\sin(x)$ y $x$ para cuando $x\rightarrow 0$ basandonos en $\sin(x)\leqslant x \leqslant\tan(x)$. En general podemos decir que $\sin(f(x))$ y $f(x)$ son equivalentes si $f(x)$ es un infinitésimo cuando $x\rightarrow c.$

. . .

:::{.note data-label="Definición de $\e$"}
$$
\e  = \lim_{x\rightarrow \infty} \Big(1 + \frac{1}{x}\Big)^x
$$
:::

A partir de la definición de $\e$, valido también para cualquier función que tienda a infinito si $x\rightarrow \infty$, tenemos que
$$
1 = \ln\Big(\lim_{x\rightarrow \infty} \Big(1 + \frac{1}{x}\Big)^x\Big) = \lim_{x\rightarrow \infty} x\ln\Big(1 + \frac{1}{x}\Big) = \lim_{x\rightarrow \infty}\frac{\ln(1 + 1/x)}{1/x}.
$$
Podemos generalizar y decir que el límite $\lim_{x\rightarrow c} \ln(1+f(x))$ es equivalente a $\lim_{x\rightarrow c} f(x)$, si y solo si $f(x)$ es un infinitésimo cuando $x\rightarrow c.$

---

$$
\lim_{x\rightarrow 0 } \frac{x}{\sqrt{1+x}-1} =2
$$
De este límite concluimos que $\dfrac{y}{2} \simeq \sqrt{1+y}-1$, si y solo si $y = f(x)$ es un infinitésimo cuando $x\rightarrow c.$

**Lista de equivalencias para cuando $y=f(x)$ es un infinitésimo**

$$y \simeq \sin(y) \simeq \tan(y) \simeq \arcsin(y) \simeq \arctan(y) \simeq \ln(y+1) \simeq \e^{y}-1,$$

De manera similar se obtiene junto con, $1 = \sin^2(y) + \cos^2(y)$ $= y^2 + \cos^2(y),$ 

$$1-\dfrac{y^2}{2}\simeq\cos(y)$$

> Las equivalencias se pueden substituir en _productos_ o _cocientes_ únicamente. 

### Equivalencias: Infinitos

A parte del orden que hemos presentado anteriormente tenemos que

* Formula de Stirling: $n!\simeq\e^{-n}n^n\sqrt{2\pi n}$

* Clasificación de infinitos: $(\ln(\infty))^r \ll \infty^s \ll \e^\infty \ll \infty^\infty$ 
