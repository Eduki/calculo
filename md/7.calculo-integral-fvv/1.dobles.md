# Integrales dobles

Sea $f(x,y)$ una función de dos variables y $R\in \dom(f)$ una región para la cual definimos la integral doble
$$
V = \iint\limits_R f(x,y)\dd{x}\dd{y}.
$$
La integral representa el volumen definido por la superficie $f(x,y)$, el plano $z=0$ y la región $R.$

--- 

Al igual que las integrales definidas de funciones reales de una variable real, para calcular dicho volumen podemos dividir la región $R$ en $n$ subregiones $\Delta S_i$ regulares.

Tenemos que 

$$
V_n = \sum_{i=1}^n f(x_i,y_i) \Delta S_i.
$$

$V_n$ se aproxima al volumen real a medida que cogemos particiones más y más pequeñas.

Cogiendo el límite $n\rightarrow \infty$,
$$
V = \iint\limits_R f(x,y)\dd{x}\dd{y} = \lim_{n\rightarrow \infty} \sum_{i=1}^n f(x_i,y_i) \Delta S_i.
$$

[Si la función $f(x,y)$ es continua en $R$, la integral doble $\iint_{R} f(x,y) \dd{x}\dd{y}$ existe.
 ]{.theorem data-label="Teorema: "}

## Propiedades

1. $\ds\iint\limits_{R} \qty[f(x,y) \pm g(x,y)] \dd{x}\dd{y}$ 
$=\iint\limits_{R} f(x,y)\dd{x}\dd{y} \pm \iint\limits_{R}g(x,y) \dd{x}\dd{y}.$

1. $\ds\iint\limits_{R} kf(x,y) \dd{x}\dd{y}$ $= k\iint\limits_{R} f(x,y)\dd{x}\dd{y}.$
1. Si $f(x,y)\geqslant 0$ para $(x,y)\in R,$ entonces $\ds\iint\limits_{R} f(x,y) \dd{x}\dd{y} \geqslant 0.$ 
1. Para $R = R_1 \cup R_2$ donde $R_1 \cap R_2$ es el borde que separa las dos regiones, 
    $$
    \iint\limits_R f(x,y)\dd{x}\dd{y} = \iint\limits_{R_1} f(x,y)\dd{x}\dd{y} + \iint\limits_{R_2} f(x,y)\dd{x}\dd{y}.
    $$

