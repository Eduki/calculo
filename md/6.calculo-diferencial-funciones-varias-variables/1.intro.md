Las funciones reales de varias variables reales extienden la noción de función de una variable,
$$
\begin{aligned}
    f:\mathbb{R}^n & \longrightarrow \mathbb{R} \\
    x_1,\dots,x_n &\longmapsto z = f(x_1,\dots,x_n)
\end{aligned}
$$
donde $x_1,\dots,x_n$ son variables independientes y $z$ es la variable dependiente.