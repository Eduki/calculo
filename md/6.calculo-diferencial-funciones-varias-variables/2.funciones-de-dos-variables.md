# Funciones de dos variables

$$
\begin{aligned}
    f:\mathbb{R}^2 & \longrightarrow \mathbb{R} \\
    x, y &\longmapsto z = f(x,y)
\end{aligned}
$$

## Dominio e imagen
El dominio de una función de dos variables está definido para cualquier conjunto de pares $(x,y)$ para los que está definida la función,
$$
\dom(f) = \{ (x,y)\in\mathbb{R} \mid f(x, y)\in \mathbb{R}\}.
$$

La imagen o recorrido se define como el conjunto de valores obtenidos a partir de la función evaluada en todo el dominio,
$$
\im(f) = \{f(x,y) \mid (x,y)\in\dom(f) \}.
$$

## Intervalos y entornos

Un intervalo rectangular _abierto_ se define como
$$
    A = \{(x,y)\in\mathbb{R}^2 \mid a < x < b \land c<y<d\}
$$

Un intervalo rectangular _cerrado_ se define como
$$
    A = \{(x,y)\in\mathbb{R}^2 \mid a \leqslant x \leqslant b \land c<y<d\}
$$

Un entorno rectangular centrado en el punto $P(a,b)$ se define como 
$$
    A = \{(x,y)\in\mathbb{R}^2 \mid \abs{x-a}<\delta_1 \land  \abs{y-a}<\delta_2 \}
$$
para $\delta_1, \delta_2>0$. Si $\delta_1 = \delta_2$ el entorno se llama cuadrangular.

Un entorno circular centrado en el punto $P(a,b)$ se define como
$$
    A = \{(x,y)\in\mathbb{R}^2 \mid \sqrt{(x-a)^2+ (y-b)^2} < \delta\}
$$

Si de estos entornos excluimos el punto $P(a,b)$ se llaman entornos reducidos.

## Representaciones 

Al conjunto de puntos tridimensionales
$$
G = \{(x,y,z)\in\mathbb{R}^3 \mid z = f(x,y) \}
$$
se le conoce como _gráfica_ de la función, y es una **superficie**.

Para diferentes valores constantes de una de las variables se obtienen las _secciones_
$$
\begin{aligned}
Z_{y_0} & = \{(x,z)\in\mathbb{R}^2 \mid z=f(x,y_0)\} \Rightarrow f_{y_0}(x),\\
Z_{x_0} & = \{(y,z)\in\mathbb{R}^2 \mid z=f(x_0,y)\} \Rightarrow f_{x_0}(y).
\end{aligned}
$$
Las secciones principales se definen para $x_0=0$ o $y_0=0$.

Las curvas de nivel se definen como el conjunto de valores $(x,y)$ para los que la función obtiene un mismo valor
$$
C_z=\{ (x,y) \in \mathbb{R}^2 \mid f(x,y) = z \}
$$

## Ejercicios 

1. Hallar el dominio de las siguientes funciones:

    - $f(x,y) = \dfrac{\ln(y+x)}{xy} + \arccos\dfrac{x}{2}$

    - $f(x,y) = \dfrac{1}{\sqrt{9-x^2-9y^2}} + \ln(x-y-1)$
    - $f(x,y) = \dfrac{\ln(x^2-2x+y^2)}{\e^{\frac{1}{x^2-y^2}}}$
    - $f(x,y) = \begin{cases}\dfrac{xy-2x +y^2}{2x+y^2}&\text{si } x<0,\\
    \sqrt{4-x^2+y^2}& \text{si } x\geqslant 0.\end{cases}$
    - $f(x,y) = \ln\dfrac{x}{y} + \arcsin(2x^2+y^2-1)$ 
    - $f(x,y,z) = \dfrac{1}{\sqrt{25 - x^2 - y^2 - z^2}}$

