---
itinerary:
  A:
    rm: "1.4,1.5,2.3,2.4,2.20,3.4,3.5,3.7,3.20,4.20,6.5,6.20,5.6,5.20"
    perm: "2.0|2.10,5.0|5.15,6.0|6.15,5.|6.,7.0|7.10"
  B:
    rm: "2.10,5.15,6.,7."
    perm: "2.0|2.20,3.0|3.20,4.0|4.20,5.0|5.20"
---

<!-- Home file! This should be used as index.html or homepage
     Note that config.md is also loaded
-->

### Lista de itinerarios

[
  [Ingeniería AUTOMOCIÓN (DUAL)]{.title}
  ](A){.card}

[
  <!-- ![Hello](/img/ardatzean_transformazioa.png) -->
  [Ingeniería QUÍMICA + ELECTRÓNICA Y AUTOMÁTICA + MECÁNICA-ELECTRÓNICA INDUSTRIAL]{.title}
  ](B){.card}

### Agradecimientos

A los alumnos de 1º de Ingeniería Automoción (Dual) y de 1º de Ingenierías de Química, Electrónica y Mecánica-Electrónica Industriales de la Escuela de Ingeniería de Vitoria-Gasteiz del curso 2023-2024, por sus aportaciones y su paciencia.

:::{.flushright}
Jagoba Apellaniz
:::