---
title: Cálculo
# author: Iagoba Apellaniz <iagoba.apellaniz@gmail.com>
lang: es-ES
# CROSSREF
linkReferences: true
chapters: true
chaptersDepth: 1
figureTitle: Figura
figPrefix:
    - fig.
    - figs.
figureTemplate: "**$$figureTitle$$ $$i$$$$titleDelim$$** $$t$$"
tableTitle: Tabla
eqnPrefix:
    - ec.
    - ecs.
eqnPrefixTemplate: $$p$$&nbsp;($$i$$)
# LATEX EQUATIONS
header-includes: |
    \newcommand{\ds}{\displaystyle}
    \newcommand{\fg}{\!\!\!\:}
    \newcommand{\e}{\mathrm{e}}
    \newcommand{\i}{\mathrm{i}}
    \newcommand{\rpar}{\right)}
    \newcommand{\lpar}{\left(}
    \newcommand{\leqs}{\leqslant}
    \newcommand{\geqs}{\geqslant}
    \DeclareMathOperator{\sgn}{sgn}
    \DeclareMathOperator{\re}{Re}
    \DeclareMathOperator{\im}{Im}
    \DeclareMathOperator{\dom}{Dom}
    \DeclareMathOperator{\arsinh}{arsinh}
    \DeclareMathOperator{\arcosh}{arcosh}
    \DeclareMathOperator{\artanh}{artanh}
---