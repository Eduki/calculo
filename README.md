# Eduki - Calculo

## VSCode - Docker (Pandoc y filtros, Latex, Julia)

Para desarrollar apuntes mediante la plataforma Eduki, es recomendable utilizar VSCode y Docker.
De esta manera evitar el proceso de instalación de las diferentes dependencias (software).

## Blocks

DIVs in HTML <-> tcolorbox environtment or default environtments in LaTeX

### Notes
`
.note type=["alert", "warning", "example"] title="The title"
`

```
<div class="note" type="example" toggle="false" title="The title">
</div>

\begin{mynote}[example]{The title}
    #1
\end{mynote-example}
```

### Theorems

.theorem type=[] title="Regla de Barrow"

<div class="theorem" type="Lema" title="Regla de Barrow">
</div>

\begin{theorem}{The title}
    #1
\end{theorem}

### Exercises

.exercise 

<div class="exercise" title="Regla de Barrow">
</div>

## Inlines

SPANs in HTML <-> newcommands or default commands in LaTeX

### Hints

`.hint`` 

```
<span class="hint"> </span>

\hint{#1}
```


